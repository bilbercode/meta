<h1>Neutrino <small>Meta</small></h1>

<h2>Installation<h2>
<pre>npm install git+https://git.eckoh.com/neutrino/meta.git</pre>
<p>

<h2>Goals</h2>
<p>
Neutrino Meta is a simple wrapper for <a href="https://github.com/rbuckton/reflect-metadata">rbuckton/reflect-metadata</a> please refer 
rbuckton/reflect-metadata for more information on how reflect operates. It purpose is to stablise
 the internal API for the Neutrino core and libraries.
 </p>
 
 <h2>API</h2>
 
 Please refer to <a href="https://git.eckoh.com/pages/neutrino/meta/classes/metadata.html">the generated documentation</a>
