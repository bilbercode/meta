"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
require("source-map-support/register");
var Metadata = (function () {
    function Metadata() {
    }
    /**
     * Store some metadata against the target
     *
     * @param {string|symbol} key The key to store the metadata under.
     * @param {any} metadata The metadata to set
     * @param {FunctionConstructor|Function} target The function to store the metadata against
     * @param {string|symbol} targetKey The targets key to store the data against.
     */
    Metadata.set = function (key, metadata, target, targetKey) {
        Reflect.defineMetadata(key, metadata, typeof target["prototype"] !== "undefined" ?
            target : target.constructor, targetKey);
    };
    /**
     * Check if the target has any metadata attributed to it
     *
     * @param {string|symbol} key The key to store the metadata under.
     * @param {FunctionConstructor|Function} target The function to store the metadata against
     * @param {string|symbol} targetKey The targets key to store the data against.
     */
    Metadata.has = function (key, target, targetKey) {
        return Reflect.hasMetadata(key, typeof target["prototype"] !== "undefined" ?
            target : target.constructor, targetKey);
    };
    /**
     * Get any metadata attributed to the target
     *
     * @param {string|symbol} key The key to store the metadata under.
     * @param {FunctionConstructor|Function} target The function to store the metadata against
     * @param {string|symbol} targetKey The targets key to store the data against.
     */
    Metadata.get = function (key, target, targetKey) {
        return Reflect.getMetadata(key, target.prototype ? target : target.constructor, targetKey);
    };
    /**
     * Remove any metadata attributed to the target
     *
     * @param {string|symbol} key The key to store the metadata under.
     * @param {FunctionConstructor|Function} target The function to store the metadata against
     * @param {string|symbol} targetKey The targets key to store the data against.
     */
    Metadata.remove = function (key, target, targetKey) {
        return Reflect.deleteMetadata(key, target, targetKey);
    };
    /**
     * Store some metadata against the target
     *
     * @param {string|symbol} key The key to store the metadata under.
     * @param {any} metadata The metadata to set
     * @param {FunctionConstructor|Function} target The function to store the metadata against
     * @param {string|symbol} targetKey The targets key to store the data against.
     */
    Metadata.prototype.set = function (key, metadata, target, targetKey) {
        Metadata.set(key, metadata, target, targetKey);
    };
    /**
     * Check if the target has any metadata attributed to it
     *
     * @param {string|symbol} key The key to store the metadata under.
     * @param {FunctionConstructor|Function} target The function to store the metadata against
     * @param {string|symbol} targetKey The targets key to store the data against.
     */
    Metadata.prototype.has = function (key, target, targetKey) {
        return Metadata.has(key, target, targetKey);
    };
    /**
     * Get any metadata attributed to the target
     *
     * @param {string|symbol} key The key to store the metadata under.
     * @param {FunctionConstructor|Function} target The function to store the metadata against
     * @param {string|symbol} targetKey The targets key to store the data against.
     */
    Metadata.prototype.get = function (key, target, targetKey) {
        return Metadata.get(key, target, targetKey);
    };
    /**
     * Remove any metadata attributed to the target
     *
     * @param {string|symbol} key The key to store the metadata under.
     * @param {FunctionConstructor|Function} target The function to store the metadata against
     * @param {string|symbol} targetKey The targets key to store the data against.
     */
    Metadata.prototype.remove = function (key, target, targetKey) {
        return Metadata.remove(key, target, targetKey);
    };
    return Metadata;
}());
exports.Metadata = Metadata;
//# sourceMappingURL=metadata.js.map