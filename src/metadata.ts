require("reflect-metadata");
import "source-map-support/register";

export class Metadata {

  /**
   * Store some metadata against the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {any} metadata The metadata to set
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  static set(key: string | symbol,
             metadata: any,
             target: FunctionConstructor|Function|symbol,
             targetKey?: string | symbol): void {

    Reflect.defineMetadata(key, metadata, typeof target["prototype"] !== "undefined" ?
      target : target.constructor, targetKey);
  }

  /**
   * Check if the target has any metadata attributed to it
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  static has(key: string | symbol,
             target: FunctionConstructor|Function|symbol,
             targetKey?: string | symbol): boolean {

    return Reflect.hasMetadata(key, typeof target["prototype"] !== "undefined" ?
      target : target.constructor, targetKey);
  }

  /**
   * Get any metadata attributed to the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  static get<T>(key: string | symbol,
             target: FunctionConstructor|Function,
             targetKey?: string | symbol): T {

    return Reflect.getMetadata(key, target.prototype ? target : target.constructor, targetKey);
  }
  /**
   * Remove any metadata attributed to the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  static remove(key: string | symbol,
                target: FunctionConstructor|Function,
                targetKey?: string|symbol): boolean {
    return Reflect.deleteMetadata(key, target, targetKey);
  }

  /**
   * Store some metadata against the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {any} metadata The metadata to set
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  set(key: string | symbol,
             metadata: any,
             target: FunctionConstructor|Function|symbol,
             targetKey?: string | symbol): void {

    Metadata.set(key, metadata, target, targetKey);
  }

  /**
   * Check if the target has any metadata attributed to it
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  has(key: string | symbol,
             target: FunctionConstructor|Function|symbol,
             targetKey?: string | symbol): boolean {

    return  Metadata.has(key, target, targetKey);
  }

  /**
   * Get any metadata attributed to the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  get<T>(key: string | symbol,
             target: FunctionConstructor|Function,
             targetKey?: string | symbol): T {

    return Metadata.get<T>(key, target, targetKey);
  }

  /**
   * Remove any metadata attributed to the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  remove(key: string | symbol,
                target: FunctionConstructor|Function,
                targetKey?: string|symbol): boolean {
    return Metadata.remove(key, target, targetKey);
  }
}
