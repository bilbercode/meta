/**
 * @example
 * <pre>
 *
 * import { MyClass } from "./my-location";
 * import { Metadata } from "neutrino-meta";
 *
 * const DESIGN_PARAM = "design:paramtypes";
 *
 * const CONSTRUCTOR_ARGUMENTS = Metadata.has(DESIGN_PARAM, MyClass) ?
 *  Metadata.get(DESIGN_PARAM, MyClass) : []
 *
 * </pre>
 */
export class Metadata {

  /**
   * Store some metadata against the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {any} metadata The metadata to set
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  static set(key: string | symbol,
             metadata: any,
             target: Function|symbol,
             targetKey?: string | symbol): void;

  /**
   * Check if the target has any metadata attributed to it
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  static has(key: string | symbol,
             target: Function|symbol,
             targetKey?: string | symbol): boolean;

  /**
   * Get any metadata attributed to the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  static get<T>(key: string | symbol,
             target: Function,
             targetKey?: string | symbol): T;

  /**
   * Remove any metadata attributed to the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  static remove(key: string | symbol,
                target: Function,
                targetKey?: string | symbol): boolean;
  /**
   * Store some metadata against the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {any} metadata The metadata to set
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  set(key: string | symbol,
      metadata: any,
      target: Function|symbol,
      targetKey?: string | symbol): void;

  /**
   * Check if the target has any metadata attributed to it
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  has(key: string | symbol,
      target: Function|symbol,
      targetKey?: string | symbol): boolean;

  /**
   * Get any metadata attributed to the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  get<T>(key: string | symbol,
      target: Function,
      targetKey?: string | symbol): T;

  /**
   * Remove any metadata attributed to the target
   *
   * @param {string|symbol} key The key to store the metadata under.
   * @param {FunctionConstructor|Function} target The function to store the metadata against
   * @param {string|symbol} targetKey The targets key to store the data against.
   */
  remove(key: string | symbol,
         target: Function,
         targetKey?: string | symbol): boolean;
}
